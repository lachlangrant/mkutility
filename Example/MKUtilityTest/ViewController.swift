//
//  ViewController.swift
//  MKUtilityTest
//
//  Created by Lachlan Grant on 6/1/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import MKUtility

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .blue
    }

}

