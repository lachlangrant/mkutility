Pod::Spec.new do |s|
  s.name             = 'MKUtility'
  s.version          = '1.1.0'
  s.summary          = 'MKSuite Utility'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://bitbucket.org/lachlangrant/mkutility/src/master/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lachlangrant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/mkutility.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKUtilityKitPod/Classes/**/*'
end
