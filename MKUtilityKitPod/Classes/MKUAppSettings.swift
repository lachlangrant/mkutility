//
//  MKUAppSettings.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

/// MKUAppSettings
public class MKUAppSettings {
	/// Singleton
	public static let shared = MKUAppSettings()
	/// RAW Info.plist Data
	public var infoDictionary: [String: Any]
	/// App Version
	public var version: String
	/// App Build
	public var build: String
	/// App BundleID
	public var bundleID: String
	/// App Display Name
	public var displayName: String
	/// Debug Build
	public var isDebugBuild: Bool
    
    public var hasDebuggerAttached: Bool

	/// Init
	public init() {
		infoDictionary = Bundle.main.infoDictionary!
		version = infoDictionary["CFBundleShortVersionString"] as! String
		build = infoDictionary["CFBundleVersion"] as! String
		bundleID = infoDictionary["CFBundleIdentifier"] as! String
		displayName = infoDictionary["CFBundleName"] as! String
        isDebugBuild = infoDictionary["MKUDebugBuild"] as? Bool ?? false
        
        hasDebuggerAttached = false
        
        hasDebuggerAttached = self.debuggerAttached()
	}
    
    private func debuggerAttached() -> Bool {
        
        var info = kinfo_proc()
        var mib : [Int32] = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        var size = MemoryLayout<kinfo_proc>.stride
        let junk = sysctl(&mib, UInt32(mib.count), &info, &size, nil, 0)
        assert(junk == 0, "sysctl failed")
        return (info.kp_proc.p_flag & P_TRACED) != 0
    }

	/// Get Data from Info.plist using Key
	///
	/// - Parameter key: Info.plist Key
	/// - Returns: Value for Key
	public func getData(key: String) -> AnyObject {
		return infoDictionary[key] as AnyObject
	}
}
