//
//  MKUAsync.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

private enum GCD {
    case main, userInteractive, userInitiated, utility, background, custom(queue: DispatchQueue)

    var queue: DispatchQueue {
        switch self {
        case .main: return .main
        case .userInteractive: return .global(qos: .userInteractive)
        case .userInitiated: return .global(qos: .userInitiated)
        case .utility: return .global(qos: .utility)
        case .background: return .global(qos: .background)
        case .custom(let queue): return queue
        }
    }
}

private class Reference<T> {
    var value: T?
}

public typealias MKUAsync = MKUAsyncBlock<Void, Void>

public struct MKUAsyncBlock<In, Out> {

    private let block: DispatchWorkItem

    private let input: Reference<In>?
    private let output_: Reference<Out>
    public var output: Out? {
        return output_.value
    }

    private init(_ block: DispatchWorkItem, input: Reference<In>? = nil, output: Reference<Out> = Reference()) {
        self.block = block
        self.input = input
        self.output_ = output
    }

    @discardableResult
    public static func main<O>(after seconds: Double? = nil, _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsyncBlock.async(after: seconds, block: block, queue: .main)
    }

    @discardableResult
    public static func userInteractive<O>(after seconds: Double? = nil,
                                          _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsyncBlock.async(after: seconds, block: block, queue: .userInteractive)
    }

    @discardableResult
    public static func userInitiated<O>(after seconds: Double? = nil,
                                        _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsync.async(after: seconds, block: block, queue: .userInitiated)
    }

    @discardableResult
    public static func utility<O>(after seconds: Double? = nil,
                                  _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsync.async(after: seconds, block: block, queue: .utility)
    }

    @discardableResult
    public static func background<O>(after seconds: Double? = nil,
                                     _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsync.async(after: seconds, block: block, queue: .background)
    }

    @discardableResult
    public static func custom<O>(queue: DispatchQueue,
                                 after seconds: Double? = nil,
                                 _ block: @escaping () -> O) -> MKUAsyncBlock<Void, O> {
        return MKUAsync.async(after: seconds, block: block, queue: .custom(queue: queue))
    }

    private static func async<O>(after seconds: Double? = nil,
                                 block: @escaping () -> O,
                                 queue: GCD) -> MKUAsyncBlock<Void, O> {
        let reference = Reference<O>()
        let block = DispatchWorkItem(block: {
            reference.value = block()
        })

        if let seconds = seconds {
            let time = DispatchTime.now() + seconds
            queue.queue.asyncAfter(deadline: time, execute: block)
        } else {
            queue.queue.async(execute: block)
        }
        return MKUAsyncBlock<Void, O>(block, output: reference)
    }

    @discardableResult
    public func main<O>(after seconds: Double? = nil, _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .main)
    }

    @discardableResult
    public func userInteractive<O>(after seconds: Double? = nil,
                                   _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .userInteractive)
    }

    @discardableResult
    public func userInitiated<O>(after seconds: Double? = nil,
                                 _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .userInitiated)
    }

    @discardableResult
    public func utility<O>(after seconds: Double? = nil,
                           _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .utility)
    }

    @discardableResult
    public func background<O>(after seconds: Double? = nil,
                              _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .background)
    }

    @discardableResult
    public func custom<O>(queue: DispatchQueue,
                          after seconds: Double? = nil,
                          _ chainingBlock: @escaping (Out) -> O) -> MKUAsyncBlock<Out, O> {
        return chain(after: seconds, block: chainingBlock, queue: .custom(queue: queue))
    }

    public func cancel() {
        block.cancel()
    }

    @discardableResult
    public func wait(seconds: Double? = nil) -> DispatchTimeoutResult {
        let timeout = seconds
                .flatMap {
            DispatchTime.now() + $0
        }
                ?? .distantFuture
        return block.wait(timeout: timeout)
    }

    private func chain<O>(after seconds: Double? = nil,
                          block chainingBlock: @escaping (Out) -> O,
                          queue: GCD) -> MKUAsyncBlock<Out, O> {
        let reference = Reference<O>()
        let dispatchWorkItem = DispatchWorkItem(block: {
            reference.value = chainingBlock(self.output_.value!)
        })

        let queue = queue.queue
        if let seconds = seconds {
            block.notify(queue: queue) {
                let time = DispatchTime.now() + seconds
                queue.asyncAfter(deadline: time, execute: dispatchWorkItem)
            }
        } else {
            block.notify(queue: queue, execute: dispatchWorkItem)
        }

        return MKUAsyncBlock<Out, O>(dispatchWorkItem, input: self.output_, output: reference)
    }
}

public struct Apply {

    public static func userInteractive(_ iterations: Int, block: @escaping (Int) -> ()) {
        GCD.userInteractive.queue.async {
            DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
        }
    }

    public static func userInitiated(_ iterations: Int, block: @escaping (Int) -> ()) {
        GCD.userInitiated.queue.async {
            DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
        }
    }

    public static func utility(_ iterations: Int, block: @escaping (Int) -> ()) {
        GCD.utility.queue.async {
            DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
        }
    }

    public static func background(_ iterations: Int, block: @escaping (Int) -> ()) {
        GCD.background.queue.async {
            DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
        }
    }

    public static func custom(queue: DispatchQueue, iterations: Int, block: @escaping (Int) -> ()) {
        queue.async {
            DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
        }
    }
}

public struct MKUAsyncGroup {

    private var group: DispatchGroup

    public init() {
        group = DispatchGroup()
    }

    private func async(block: @escaping @convention(block) () -> Swift.Void, queue: GCD) {
        queue.queue.async(group: group, execute: block)
    }

    public func enter() {
        group.enter()
    }

    public func leave() {
        group.leave()
    }

    public func main(_ block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .main)
    }

    public func userInteractive(_ block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .userInteractive)
    }

    public func userInitiated(_ block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .userInitiated)
    }

    public func utility(_ block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .utility)
    }

    public func background(_ block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .background)
    }

    public func custom(queue: DispatchQueue, block: @escaping @convention(block) () -> Swift.Void) {
        async(block: block, queue: .custom(queue: queue))
    }

    @discardableResult
    public func wait(seconds: Double? = nil) -> DispatchTimeoutResult {
        let timeout = seconds
                .flatMap {
            DispatchTime.now() + $0
        }
                ?? .distantFuture
        return group.wait(timeout: timeout)
    }
}

public extension qos_class_t {
    var description: String {
        get {
            switch self {
            case qos_class_main(): return "Main"
            case DispatchQoS.QoSClass.userInteractive.rawValue: return "User Interactive"
            case DispatchQoS.QoSClass.userInitiated.rawValue: return "User Initiated"
            case DispatchQoS.QoSClass.default.rawValue: return "Default"
            case DispatchQoS.QoSClass.utility.rawValue: return "Utility"
            case DispatchQoS.QoSClass.background.rawValue: return "Background"
            case DispatchQoS.QoSClass.unspecified.rawValue: return "Unspecified"
            default: return "Unknown"
            }
        }
    }
}

public extension DispatchQoS.QoSClass {

    var description: String {
        get {
            switch self {
            case DispatchQoS.QoSClass(rawValue: qos_class_main())!: return "Main"
            case .userInteractive: return "User Interactive"
            case .userInitiated: return "User Initiated"
            case .default: return "Default"
            case .utility: return "Utility"
            case .background: return "Background"
            case .unspecified: return "Unspecified"
            }
        }
    }
}
