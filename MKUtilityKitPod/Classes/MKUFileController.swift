//
//  MKUFileController.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

/// File Controller
public class MKUFileController {
	/// Junk Init
	public init() {
	}

	/// Get file path if file is located in main bundle
	///
	/// - Parameters:
	///   - name: File Name
	///   - type: File Extension eg .plist
	/// - Returns: File Path
	/// - Throws: MKUFileError
	public func getFilePathInMainBundle(withName name: String, ofType type: String) throws -> String {
		guard let path = Bundle.main.path(forResource: name, ofType: type) else {
			throw MKUFileError.pathError
		}

		return path
	}

	
	/// Get File Path from alternative bundle
	///
	/// - Parameters:
	///   - name: File Name
	///   - type: File Extension eg .plist
	///   - bundleID: Alternative Bundle Identifier
	/// - Returns: File Path
	/// - Throws: MKUFileError
	public func getPath(withName name: String,
						ofType type: String,
						fromBundleWithID bundleID: String) throws -> String {
		let bundle = Bundle(identifier: bundleID)

		guard let path = bundle?.path(forResource: name, ofType: type) else {
			throw MKUFileError.pathError
		}

		return path
	}

	/// Read contents of File
	///
	/// - Parameter path: File Path
	/// - Returns: Data
	/// - Throws: MKUFileError
	public func readFile(atPath path: String) throws -> Data {
		do {
			let url = URL(fileURLWithPath: path)

			do {
				let data = try Data.init(contentsOf: url)
				return data
			} catch {
				throw MKUFileError.readFileError
			}
		}
	}

	/// Wite Data to file
	///
	/// - Parameters:
	///   - data: Data to write
	///   - path: Path of file
	/// - Throws: MKUFileError
	public func write(Data data: Data, toFileAtPath path: String) throws {
		let url = URL(fileURLWithPath: path)

		do {
			try data.write(to: url)
		} catch {
			throw MKUFileError.writeFileError
		}
	}
}


/// File Errors
///
/// - pathError: Error Relating to the File Path
/// - readFileError: Error Relating to reading contents of File
/// - writeFileError: Error Relating to writing to File
public enum MKUFileError: Error {
	case pathError
	case readFileError
	case writeFileError
}
