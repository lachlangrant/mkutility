//
//  MKULog.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

/// MKULog
public class MKULog {
    /// Singleton
    public static let shared = MKULog()
    
    init() {}
    
    
    /// Debug Log
    ///
    /// - Parameter item: Log Data
    public func debug(_ item: Any) {
        if MKUAppSettings.shared.isDebugBuild {
            print("[DEBUG] \(item)")
        }
    }
    
    /// Error Log
    ///
    /// - Parameter item: Log Data
    public func error(_ item: Any) {
        print("[ERROR] \(item)")
    }
    
    /// Info Log
    ///
    /// - Parameter item: Log Data
    public func info(_ item: Any) {
        print("[INFO] \(item)")
    }
    
    
    /// Mark the Log with a seperator
    public func mark() {
        print("-------------------")
    }
}
