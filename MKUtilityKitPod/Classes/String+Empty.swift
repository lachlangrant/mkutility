//
//  String+Empty.swift
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

public extension String {
    
    public var isEmptyOrWhiteSpace: Bool {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
}

public protocol OptionalString {}
extension String: OptionalString {}

public extension Optional where Wrapped: OptionalString {
    
    public var isNilOrEmpty: Bool {
        return ((self as? String) ?? "").isEmpty
    }
}
