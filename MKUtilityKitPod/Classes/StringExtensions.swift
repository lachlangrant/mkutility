//
//  StringExtensions.swift
//  MKUtilityKit_iOS
//
//  Created by Lachlan Grant on 11/9/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

extension String {
    
    // Trim
//    var trim: String {
//        
//        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//        
//    }
    
    // Remove extra white spaces
    var extendedTrim: String {
        
        let components = self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ").trim()
        
    }
    
    // Decode HTML entities
    var decoded: String {
        
        let encodedData = self.data(using: String.Encoding.utf8)!
        let attributedOptions: [NSAttributedString.DocumentReadingOptionKey: AnyObject] =
            [
                .documentType: NSAttributedString.DocumentType.html as AnyObject,
                .characterEncoding: String.Encoding.utf8 as AnyObject
        ]
        
        do {
            
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            
            return attributedString.string
            
        } catch _ {
            
            return self
            
        }
        
    }
    
    // Strip tags
    var tagsStripped: String {
        
        return self.deleteTagByPattern(MKULinkRegex.rawTagPattern)
        
    }
    
    // Delete tab by pattern
    func deleteTagByPattern(_ pattern: String) -> String {
        
        return self.replacingOccurrences(of: pattern, with: "", options: .regularExpression, range: nil)
        
    }
    
    // Check if it's a valid url
    func isValidURL() -> Bool {
        
        return MKULinkRegex.test(self, regex: MKULinkRegex.rawUrlPattern)
        
    }
    
    // Check if url is an image
    func isImage() -> Bool {
        
        return MKULinkRegex.test(self, regex: MKULinkRegex.imagePattern)
        
    }
    
}
