//
//  URLSessionExtension.swift
//  MKUtilityKit_iOS
//
//  Created by Lachlan Grant on 11/9/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

public extension URLSession {
    
    // Synchronous request to get the real URL
    public func synchronousDataTaskWithURL(_ url: URL) -> (Data?, URLResponse?, NSError?) {
        
        var data: Data?, response: URLResponse?, error: NSError?
        let semaphore = DispatchSemaphore(value: 0)
        
        dataTask(with: url, completionHandler: {
            
            data = $0; response = $1; error = $2 as NSError?
            semaphore.signal()
            
        }) .resume()
        
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return (data, response, error)
        
    }
    
}
