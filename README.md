# MKUtilityKitPod

[![CI Status](http://img.shields.io/travis/lachlangrant/MKUtilityKitPod.svg?style=flat)](https://travis-ci.org/lachlangrant/MKUtilityKitPod)
[![Version](https://img.shields.io/cocoapods/v/MKUtilityKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUtilityKitPod)
[![License](https://img.shields.io/cocoapods/l/MKUtilityKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUtilityKitPod)
[![Platform](https://img.shields.io/cocoapods/p/MKUtilityKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUtilityKitPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKUtilityKitPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKUtilityKitPod'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKUtilityKitPod is available under the MIT license. See the LICENSE file for more info.
